// fribs - data model
// Loaded on both client and server
//

/*
 Ideas for next release:
 - join, leave bet 
 - instead of equal contributions, allow user-defined contribution to the bet
 - payment to bet can still be made after <waitingDays> days but charge interest (similar to
 	credit cards' 55 days interest free period)
 - 
*/

/*

   Add to existing Meteor.User model
   	friends: uids[]
   	inReqs: uids[]
   	outReqs: uids[]
   	flimit: int //set to 1 for the moment
   	flen: int //number of friends == len(friends)
   	notifications: string[]
   	bets: {bid,amount}[] //amount promised
   	betReqs: bids[]
   	balance: Number //$$ in user's account

	topUpBalance()

   	
   Bets
   	creator: uid //one who created, mainly for account-keeping purposes
   	public: boolean
   	sideA: uids[]
   	sideB: uids[]
   	description: string
   	result: A|B|D (A sideA, B sideB, D draw)
   	active: boolean
	stake: int
	invited: int
	accepted: int
	rejected: int

	createNewBet()
	acceptBetReq()
	rejectBetReq()
	activateBet()
	voteWinner()
	closeBet()

   Commission
	total: int
	bets: {bid, date, fees}[]

----------------------------------------------------------------------------------
	Betting algorithms:
	
	result;			
	while(true)
		result = vote(user, side)
		if(result)
			break;
	if(result == truth)
		notifyLside() //losing side gets 1 week to pay into the bet
		distributeMoney() //to winning side
		updateCreditRating() //update everyone's credit rating
	else if(result == lie)
		reduceCreditRating() //no 2nd chance
	closeBet()
	
----------------------------------------------------------------------------------

*/

Bets = new Meteor.Collection("bets");
if(Meteor.isServer){
	Commission = new Meteor.Collection("commission");
}

//do not allow any cowboy insert, update, remove
Bets.allow({
	insert: function(userId, bet){
		return false;
	},
	update: function(userId, bet, fields, modifier){
		return false;
	},
	remove: function(userId, bet){
		return false;
	}
});

if(Meteor.isServer){
Commission.allow({
	insert: function(userId, comm){
		return false;
	},
	update: function(userId, comm, fields, modifier){
		return false;
	},
	remove: function(userId, comm){
		return false;
	}
});
}

Meteor.users.allow({
	insert: function(userId, user){
		return false;
	},
	update: function(userId, user){
		return false;
	},
	remove: function(userId, user){
		return false;
	}
});

if(Meteor.isServer){
	var commission = Commission.findOne();
	if(!commission){
		var commId = Commission.insert({
			total: 0,
			bets: []
		});
	}
	else{
		console.log("Commission collection already exists.");
	}
}


var NonEmptyString = Match.Where(function (x) {
	check(x, String);
	return x.length !== 0;
});

var NonEmptyStringList = Match.Where(function (x){
	check(x, [NonEmptyString]);
	return x.length >= 1;
});

var ValidNumber = Match.Where(function (x) {
	check(x, Number);
	return x > 0;
});

var majority = 0.75;
//var waitingDays = 5; //days 
var waitingDays = 0.001; //FIXME: temporary days for testing
var msPerDay = 86400000;

Meteor.methods({
	confirmFriendReq: function(friendId){
		check(friendId, NonEmptyString);
		if(!this.userId){
			throw new Meteor.Error(403, "You can't accept request unless you're logged in");
		}
		var friend = Meteor.users.findOne(friendId);
		if(!friend)
			throw new Meteor.Error(404, "No such user");
		var self = Meteor.users.findOne(this.userId);
		if(!_.contains(self.inReqs, friendId))
			throw new Meteor.Error(403, "No request found");
		
		if(self.flen < self.flimit && friend.flen < friend.flimit){
			Meteor.users.update(this.userId,
							{
							 $pull: {inReqs: friendId},
							 $push: {friends: friendId},
							 $inc: {flen: 1}
							});
			Meteor.users.update(friendId,
							{
							 $pull: {outReqs: this.userId},
							 $push: {friends: this.userId},
							 $inc: {flen: 1}
							});
		
		}
		else{
			throw new Meteor.Error(403, "Friend limit reached.");
		}
	},
	sendFriendReq: function(futureFriendId){
		check(futureFriendId, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in to send a request");
		var futureFriend = Meteor.users.findOne(futureFriendId);
		if(!futureFriend)
			throw new Meteor.Error(404, "Person not found");
		var self = Meteor.users.findOne(this.userId);
		if(!_.contains(futureFriend.inReqs, this.userId) && self.flen < self.flimit){
			Meteor.users.update(futureFriendId,
								{$push: {inReqs: this.userId}});
			Meteor.users.update(this.userId,
								{$push: {outReqs: futureFriendId}});
		}
	},
	rejectFriendReq: function(friendId){
		check(friendId, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in");
		var friend = Meteor.users.findOne(friendId);
		if(!friend)
			throw new Meteor.Error(404, "No such user");
		var self = Meteor.users.findOne(this.userId);
		if(!_.contains(self.inReqs, friendId))
			throw new Meteor.Error(403, "No request found");
		Meteor.users.update(friendId,
							{$pull: {outReqs: this.userId}});
		Meteor.users.update(this.userId,
							{$pull: {inReqs: friendId}});

	},
	removeFriend: function(friendId){
		check(friendId, NonEmptyString);
		if(!this.userId)
				throw new Meteor.Error(403, "Must be logged in first");
		var friend = Meteor.users.findOne(friendId);
		if(!friend)
			throw new Meteor.Error(404, "User not found");
		var self = Meteor.users.findOne(this.userId);
		if(!self)
			throw new Meteor.Error(404, "Unable to find myself.");
		//friends
		if (_.contains(self.friends, friendId) && _.contains(friend.friends, this.userId)) {
			Meteor.users.update(friendId,
								{
								 $pull: {friends: this.userId},
								 $inc: {flen: -1}
								});
			Meteor.users.update(this.userId,
								{
								 $pull: {friends: friendId},
								 $inc: {flen: -1}
								});
		}
		//one-way friends, display sensible message
		else if (_.contains(self.friends, friendId)){
			Meteor.users.update(this.userId,
								{$pull: {friends: friendId}});
			console.log("One way friend: "+this.userId+" -> "+friendId);
		}
		//one-way friends, display message
		else if (_.contains(friend.friends, this.userId)){
			Meteor.users.update(friendId,
								{$pull: {friends: this.userId}});
			console.log("One way friend: "+friendId+" -> "+this.userId);
		}
		else{
			throw new Meteor.Error(404, "Trying to delete non existent friend");
		}

	},
	notify: function(uid, message){
		check(uid, NonEmptyString);
		check(message, String);
		if(!this.userId)
			throw new Meteor.Error(403, "Must be logged in first");
		Meteor.users.update(uid,
							{$addToSet: {notifications: message}});
	},
	unnotify: function(uid, message){
		check(uid, NonEmptyString);
		check(message, String);
		if(!this.userId)
			throw new Meteor.Error(403, "Must be logged in first");
		Meteor.users.update(uid,
							{$pull: {notifications: message}});
	},

	createNewBet: function(options){
		check(options, {
            public: Boolean,
            invited: NonEmptyStringList,  //one of A or B must contain this.userId
            description: NonEmptyString,
            stake: ValidNumber,
    	});
		if(!this.userId)
			throw new Meteor.Error(403, "Must be logged in first");
		if (options.description.length > 1000)
      		throw new Meteor.Error(413, "Description too long");

      	var betId = Bets.insert({
			creator: this.userId,
			public: options.public,
			sideA: [this.userId],
			sideB: [],
			description: options.description,
			result: "",
			active: false,
			stake: options.stake,
			invited: options.invited.length+1, //invited + creator
			accepted: 1,
			rejected: 0,
			vA: [], //votesA: uids[]
			vB: [], //votesB: uids[]
			paid: 0
		});
      	//console.log("betId = "+betId);
      	Meteor.users.update(this.userId,
							{
      						 $push: {bets: {bid: betId, amount: options.stake, due: -1},
      								 betReqsSent: betId
      								}
      						});
      	var notifyMsg = null;
      	for (var i=0, l=options.invited.length; i<l; i++) {
      		notifyMsg = displayId(this.userId)+" wants to bet with you that "+options.description;
      		Meteor.users.update(options.invited[i],
      							{
      								$push: {betReqs: betId},
      								$addToSet: {notifications: notifyMsg}
      							});
      	}
	},

	acceptBetReq: function(betId, side){
		check(betId, NonEmptyString);
		check(side, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in");
		var self = Meteor.users.findOne(this.userId);
		if(!self)
			throw new Meteor.Error(413, "Unable to locate the user");
		var bet = Bets.findOne(betId);
		if(!bet)
			throw new Meteor.Error(413, "Unable to find bet: "+betId);
		if(_.contains(bet.sideA, this.userId) || _.contains(bet.sideB, this.userId))
			throw new Meteor.Error(413, "You are already part of the bet");

		//if(bet.accepted + bet.rejected + 1 === bet.invited){
		var mySide = [];
		if(side === "a"){
			if(bet.accepted+bet.rejected+1 === bet.invited && bet.accepted === bet.sideA.length){
				throw new Meteor.Error(413, "Y'all can't be on the same side.");
			}
			mySide = bet.sideA;
			Bets.update(betId,
						{
							$addToSet: {sideA: this.userId},
							$inc: {accepted: 1}
						});
		}
		else if(side === "b"){
			if(bet.accepted+bet.rejected+1 === bet.invited && bet.accepted === bet.sideB.length){
				throw new Meteor.Error(413, "Y'all can't be on the same side.");
			}
			mySide = bet.sideB;
			Bets.update(betId,
						{
							$addToSet: {sideB: this.userId},
							$inc: {accepted: 1}
						});
		}
		else{
			throw new Meteor.Error(413, "Invalid side for the bet");
		}
		
		if(mySide.length < 0)
			throw new Meteor.Error(413, "Side has negative length");
		
		
		var betIndex;
		var user;
		var modifier;
		for(var i=0,l=mySide.length; i<l; i++){
			user = Meteor.users.findOne(mySide[i]);
			if(!user)
				throw new Meteor.Error(413, "User "+mySide[i]+" not found in db.");
			if(Meteor.isServer){
				Meteor.users.update({_id: mySide[i], "bets.bid": betId},
									{
										$set: {"bets.$.amount": bet.stake/(l+1)}
									});
			}
			else{
				betIndex = _.indexOf(_.pluck(user.bets, 'bid'), betId);
				if(betIndex != -1){
					modifier = {$set: {}};
        			modifier.$set["bets." + betIndex + ".amount"] = bet.stake/(l+1);
        			Meteor.users.update(mySide[i], modifier);
				}
				else{
					throw new Meteor.Error(413, "Bet("+betId+") has "+displayId(mySide[i])+ 
							" but he doesn't have the bet.");
				}
			}
		}
		
		Meteor.users.update(this.userId,
							{
								$push: {bets: {bid: betId, amount: bet.stake/(mySide.length+1), due: -1}},
								$pull: {betReqs: betId}
							}
		);  
		if(bet.accepted + bet.rejected + 1 === bet.invited){
			Bets.update(betId,
						{
							$set: {active: true}
						}
			);
			Meteor.users.update(bet.creator,
								{
									$pull: {betReqsSent: betId}
								}
			);
		}

	},

	rejectBetReq: function(betId){
		check(betId, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in");
		var bet = Bets.findOne(betId);
		if(!bet)
			throw new Meteor.Error(413, "Unable to find bet: "+betId);
		if(_.contains(bet.sideA, this.userId) || _.contains(bet.sideB, this.userId))
			throw new Meteor.Error(413, "You are already part of the bet");
		Meteor.users.update(this.userId,
							{
								$pull: {betReqs: betId},
							}
		);
		Bets.update(betId,
					{
						$inc: {rejected: 1}
					}
		);
		if(bet.accepted+bet.rejected+1 === bet.invited){
			Bets.update(betId,
						{
							$set: {active: true}
						}
			);
			Meteor.users.update(bet.creator,
								{
									$pull: {betReqsSent: betId}
								}
			);
			if(bet.sideA.length===0 || bet.sideB.length===0){
				closeBet(betId);
			}
		}
	},

	voteWinner: function(betId, winner){
		check(betId, NonEmptyString);
		check(winner, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in");
		var outdatedBet = Bets.findOne(betId);
		if(!outdatedBet)
			throw new Meteor.Error(413, "Unable to find bet: "+betId);

		var bothVotes = outdatedBet.vA.concat(outdatedBet.vB);
		for(var i=0, len=bothVotes.length; i<len; i++){
			if(bothVotes[i] === this.userId){
				console.log("You have already voted. Can't do it again.");
				return;
			}
		}

		if(winner === "a"){
			Bets.update(betId,
					{
						$addToSet: {vA: this.userId}
					});
		}
		else if(winner === "b"){
			Bets.update(betId,
					{
						$addToSet: {vB: this.userId}
					});
		}
		else{
			throw new Meteor.Error(413, "Invalid side chosen as winner");
		}
		
		var bet = Bets.findOne(betId);
		if(!bet)
			throw new Meteor.Error(413, "Unable to find bet: "+betId);
		var pid;
		var va = bet.vA.length;
		var vb = bet.vB.length;
		var n = bet.sideA.length;
		var m = bet.sideB.length;
		//TODO: disable once majority is achieved
		if(va > (n+m)*majority){
			Bets.update(betId,
						{
							$set: {result: "a"}
						});
			notifyLside(betId, bet.sideB);
			if(Meteor.isServer){
				pid = Meteor.setTimeout(function(){
							distributeMoney(betId);
						}, waitingDays*msPerDay);
			}
		}
		if(vb > (n+m)*majority){
			Bets.update(betId,
						{
							$set: {result: "b"}
						});
			notifyLside(betId, bet.sideA);
			if(Meteor.isServer){
			pid = Meteor.setTimeout(function(){
							distributeMoney(betId);
						}, waitingDays*msPerDay);
			}
		}
		//majority's not possible || lie
		if(((_.max([va,vb])/(n+m) + 1-((va+vb)/(n+m))) < majority) || (va===vb && va+vb===n+m)){
			var notifyMsg = "Can't decide a winner if people are not honest about who won/lost. "+
							"So reduced your credit rating.";
			for(var i=0; i<va; i++){
				Meteor.users.update(bet.vA[i],
									{
										$addToSet: {notifications: notifyMsg}
									});
			}
			for(var j=0; j<vb; j++){
				Meteor.users.update(bet.vB[j],
									{
										$addToSet: {notifications: notifyMsg}
									});
			}
			reduceCreditRating(bet.vA.concat(bet.vB));
			closeBet(betId);
		}
	},

	payBet: function(betId){
		check(betId, NonEmptyString);
		if(!this.userId)
			throw new Meteor.Error(403, "You must be logged in");
		var self = Meteor.users.findOne(this.userId);
		if(!self)
			throw new Meteor.Error(403, "Unable to locate user: "+this.userId);
		var bet = Bets.findOne(betId);
		if(!bet)
			throw new Meteor.Error(403, "Unable to locate bet: "+betId);
		//TODO: check user belongs to the losing side & the bet has a winning & losing side
		//		disallow user to press paybet multiple times
		
		if(bet.result.length === 0){
			console.log("Cannot make a payment to the bet until it's result is decided.");
			return;
		}

		var losingSide;
		if(bet.result === "a"){
			losingSide = bet.sideB;
		}
		else{
			losingSide = bet.sideA;
		}

		var present = false;
		for(var i=0, len=losingSide.length; i<len; i++){
			if(losingSide[i] === this.userId){
				present = true;
				break;
			}
		}
		if(!present){
			console.log("You cannot pay this bet because you ain't on the losing side.");
			return;
		}

		var thisBet;
		for(var j=0, len=self.bets.length; j<len; j++){
			if(present && self.bets[j].bid === betId && self.bets[j].amount === -1){
				console.log("You already paid the bet. Can't pay it again.");
				return;
			}
		}

		var betCommitment;
		var commFees;
		var betIndex;

		for (var i=0,l=self.bets.length; i<l; i++) {
			if(self.bets[i].bid === betId){
				betCommitment = self.bets[i].amount;
				commFees = (getCommissionRate(self.rating)/100)*self.bets[i].amount;
				console.log("betCommitment="+betCommitment+" commFees="+commFees);
				betCommitment += commFees;
				if(self.balance >= betCommitment){
					if(Meteor.isServer){
						Meteor.users.update({_id: this.userId, "bets.bid": betId},
											{
												$inc: {balance: -betCommitment},
												$set: {"bets.$.amount": -1} //-1 == paid
											});
					}
					else{
						betIndex = _.indexOf(_.pluck(self.bets, 'bid'), betId);
						if(betIndex != -1){
							modifier = {$inc: {balance: -betCommitment},
										$set: {}};
        					modifier.$set["bets." + betIndex + ".amount"] = -1;
        					Meteor.users.update({_id: this.userId, "bets.bid": betId}, 
        										modifier);
						}
						else{
							throw new Meteor.Error(413, "Unable to find bet '"+betId+"' in "+displayId(this.userId)
													+"'s bets.");
						}
					}
					Bets.update(betId,
								{
									$inc: {paid: (betCommitment - commFees)}
								});
					if(Meteor.isServer){
						var commission = Commission.findOne();
						if(!commission)
							throw new Meteor.Error(413, "No Commission collection found.");
						commission = Commission.findOne({_id: commission._id, "bets.bid": betId});
						if(!commission){
							commission = Commission.findOne();
							Commission.update(commission._id,
									  	  	  {
									  			$inc: {total: commFees},
									  			$push: {bets: {bid: betId, date: new Date(), fees: commFees}}
									  	  	  });
						}
						else{
							Commission.update({_id: commission._id, "bets.bid": betId},
									  	  	  {
									  			$inc: {total: commFees, "bets.$.fees": commFees}
									  	  	  });
						}
					}
				}
				else{
					//notify & show popup for topUpBalance
					Meteor.users.update(this.userId,
										{
											$addToSet: {notifications: "Insufficient funds. Top-up your balance first."}
										});
					//notify(this.userId, "Insufficient funds. Top-up your balance first.");
				}
				break;
			}
		}
	},

	topUpBalance: function(amount){
		check(amount, ValidNumber);
		if(!this.userId)
			throw new Meteor.Error(403, "Must be logged in first");
		Meteor.users.update(this.userId,
							{
							 $inc: {balance: amount}
							});
	},
});

distributeMoney = function(betId){
	var bet = Bets.findOne(betId);
	var winningSide;
	var losingSide;
	if(bet.result === "a"){
		winningSide = bet.sideA;
		losingSide = bet.sideB;
	}
	else{
		winningSide = bet.sideB;
		losingSide = bet.sideA;
	}
	var n = winningSide.length;
	var commFees;
	var totalComm = 0;
	//TODO: optimised for equal contribution. Should change it to 
	//		% contribution in later releases.
	for(var i=0; i<n; i++){
		commFees = getWinnerCommission(winningSide[i], betId);
		if(commFees === -1)
			throw new Meteor.Error(413, "Invalid commission found.");
		totalComm += commFees;
		Meteor.users.update(winningSide[i],
							{
								$inc: {balance: (bet.paid/n)-commFees}
							});
	}
	if(Meteor.isServer){
		var commission = Commission.findOne();
		if(!commission)
			throw new Meteor.Error(413, "Unable to find the commission collection");
		commission = Commission.findOne({_id: commission._id, "bets.bid": betId});
		if(!commission){
			commission = Commission.findOne();
			Commission.update(commission._id,
					  	  	  {
					  			$inc: {total: totalComm},
					  			$push: {bets: {bid: betId, date: new Date(), fees: totalComm}}
					  		  });
		}
		else{
			Commission.update({_id: commission._id, "bets.bid": betId},
					  	  	  {
					  			$inc: {total: totalComm, "bets.$.fees": totalComm}
					  		  });
		}
	}
	updateCreditRating(losingSide, betId);
	closeBet(betId);
};

getUserCommission = function(userId, betId){
	check(userId, NonEmptyString);
	check(betId, NonEmptyString);
	var user = Meteor.users.findOne(userId);
	if(!user)
		throw new Meteor.Error(413, "Unable to locate the user...");
	var commFees = -1;
	for(var i=0, l=user.bets.length; i<l; i++){
		if(user.bets[i].bid === betId){
			commFees = (getCommissionRate(user.rating)/100)*user.bets[i].amount;
			break;
		}
	}
	return commFees;
};

getWinnerCommission = function(userId, betId){
	check(userId, NonEmptyString);
	check(betId, NonEmptyString);
	var user = Meteor.users.findOne(userId);
	if(!user)
		throw new Meteor.Error(413, "Unable to locate the user...");
	var commFees = -1;
	for(var i=0, l=user.bets.length; i<l; i++){
		if(user.bets[i].bid === betId){
			commFees = (getWinnerCommissionRate(user.rating)/100)*user.bets[i].amount;
			break;
		}
	}
	return commFees;
};

notifyLside = function(betId, losingSide){
	check(losingSide, NonEmptyStringList);
	var betIndex;
	var user;
	var modifier;
	var notify_msg = "";
	var dueDate = new Date();
	dueDate.setDate(dueDate.getDate()+waitingDays);
	var bet = Bets.findOne(betId);
	if(!bet)
		throw new Meteor.Error(413, "Unable to find bet: "+betId);

	for (var i=0,l=losingSide.length; i<l; i++) {
		user = Meteor.users.findOne(losingSide[i]);
		if(!user)
			throw new Meteor.Error(413, "User "+losingSide[i]+" not found in db.");
		/*for (var j=0,m=user.bets.length; j<m; j++) {
			if(user.bets[j].bid === betId){
				notify_msg += "Please pay $"+user.bets[j].amount+" to bet("+betId+") before "+dueDate;
				Meteor.users.update(losingSide[i],
									{
										$addToSet: {notifications: notify_msg}
									});
				break;
			}
		}*/
		notify_msg = "Please pay $"+bet.stake+" to bet("+displayBetId(betId)+") before "+dueDate;
		if(Meteor.isServer){
			Meteor.users.update(
							{_id: losingSide[i], "bets.bid": betId},
							{
								$set: {"bets.$.due": dueDate},
								$addToSet: {notifications: notify_msg}
							});
		}
		else{
			betIndex = _.indexOf(_.pluck(user.bets, 'bid'), betId);
			if(betIndex != -1){
				modifier = {$set: {},
							$addToSet: {notifications: notify_msg}
						   };
       			modifier.$set["bets." + betIndex + ".due"] = dueDate;
       			Meteor.users.update(losingSide[i], modifier);
			}
			else{
				throw new Meteor.Error(413, "Bet("+betId+") contains "+displayId(losingSide[i])+ 
						" but he/she doesn't contain the bet.");
			}
		}
	}
};

updateCreditRating = function(lSide, betId){
	var user;
	var paid;
	var creditRating;
	for(var i=0, m=lSide.length; i<m; i++){
		user = Meteor.users.findOne(lSide[i]);
		if(!user)
			throw new Meteor.Error(403, "Unable to find user: "+lSide[i]);
		paid = false;
		for(var j=0, l=user.bets.length; j<l; j++){
			if(user.bets[j].bid === betId && user.bets[j].amount === -1){
				paid = true;
				break;
			}
		}
		if(user.allBetsPaid > user.allBets)
			throw new Meteor.Error(403, "Number of bets paid can't be greater than "+
											 "total number of bets.");
		if(user.allBets === -1)
			throw new Meteor.Error(403, "Number of bets can't be -1");

		if(paid){
			creditRating = getCreditRating(((user.allBetsPaid+1)/(user.allBets+1))*100);
			Meteor.users.update(lSide[i],
								{
									$inc: {allBets: 1,
										   allBetsPaid: 1},
									$set: {rating: creditRating}
								});
		}
		else{
			creditRating = getCreditRating((user.allBetsPaid/(user.allBets+1))*100);
			Meteor.users.update(lSide[i],
								{
									$inc: {allBets: 1},
									$set: {rating: creditRating}
								});
		}
	}
};

reduceCreditRating = function(defaulters){
	check(defaulters, NonEmptyStringList);
	var defaulter;
	for (var i=0, l=defaulters.length; i<l; i++){
		defaulter = Meteor.users.findOne(defaulters[i]);
		if(!defaulter)
			throw new Meteor.Error(403, "Unable to find user: "+defaulters[i]);
		if(defaulter.rating > 0){
			Meteor.users.update(defaulters[i],
								{
									$inc: {rating: -1}
								});
		}
	}
};

getCommissionRate = function(rating){
	var comRate = -1;
	if(rating === 7){
		comRate = 0.8;
	}
	else if(rating === 6){
		comRate = 1.2;
	}
	else if(rating === 5){
		comRate = 1.8;
	}
	else if(rating === 4){
		comRate = 2.7;
	}
	else if(rating === 3){
		comRate = 3.8;
	}
	else if(rating === 2){
		comRate = 5.8;
	}
	else if(rating === 1){
		comRate = 9;
	}
	else if(rating === 0){
		comRate = 15.5;
	}
	return comRate;
};

getWinnerCommissionRate = function(rating){
	var comRate = -1;
	if(rating === 7){
		comRate = 0.1;
	}
	else if(rating === 6){
		comRate = 0.2;
	}
	else if(rating === 5){
		comRate = 0.8;
	}
	else if(rating === 4){
		comRate = 1.7;
	}
	else if(rating === 3){
		comRate = 2.7;
	}
	else if(rating === 2){
		comRate = 3.4;
	}
	else if(rating === 1){
		comRate = 4.0;
	}
	else if(rating === 0){
		comRate = 5.5;
	}
	return comRate;
};

getCreditRating = function(percentage){

	var rating = -1;
	if(percentage >= 88.0 && percentage <=100.0){
		rating = 7;
	}
	else if(percentage >= 70.0 && percentage < 88.0){
		rating = 6;
	}
	else if(percentage >= 60.0 && percentage < 70.0){
		rating = 5;
	}
	else if(percentage >= 45.0 && percentage < 60.0){
		rating = 4;
	}
	else if(percentage >= 35.0 && percentage < 45.0){
		rating = 3;
	}
	else if(percentage >= 20.0 && percentage < 35.0){
		rating = 2;
	}
	else if(percentage >= 10.0 && percentage < 20.0){
		rating = 1;
	}
	else if(percentage >= 0 && percentage < 10.0){
		rating = 0;
	}
	return rating;
};

getMySide = function(userId, bet){
	if(_.contains(bet.sideA, userId) && !_.contains(bet.sideB, userId)) {
		return "a";
	}
	else if(_.contains(bet.sideB, userId) && !_.contains(bet.sideA, userId)) {
		return "b";
	}
	else{
		throw new Meteor.Error(403, "User can be present only on one side, not both or none.");	
	}
};

checkBalance = function(user, amount){
	return user.balance >= amount;
};

if(Meteor.isServer){
	Accounts.onCreateUser(function(options, user){
		if(!this.userId){
			user.friends = [];
			user.inReqs = [];
			user.outReqs = [];
			user.flimit = 1;
			user.flen = 0;
			user.notifications = [];
			user.betReqs = [];
			user.betReqsSent = [];
			user.bets = []; //active bets
			user.balance = 0;
			user.allBets = 0;
			user.allBetsPaid = 0;
			user.rating = 7; //start with AAA
			if(options.profile){
				user.profile = options.profile;
			}
			return user;
		}
	
	});
}

closeBet = function(betId){
	check(betId, NonEmptyString);
	var bet = Bets.findOne(betId);
	for(var i=0,l1=bet.sideA.length; i<l1; i++){
		Meteor.users.update(bet.sideA[i],
							{
								$pull: {bets: {bid: betId}}
							});
	}
	for(var j=0,l2=bet.sideB.length; j<l2; j++){
		//console.log("sideB[j] = "+bet.sideB[j]);
		Meteor.users.update(bet.sideB[j],
							{
								$pull: {bets: {bid: betId}}
							});
	}
	Bets.remove(betId); //TODO: see if it's a shallow or deep remove
};


displayName = function (user) {
  if (user.profile && user.profile.name)
    return user.profile.name;
  if(user){
	return user.emails[0].address;
  }
  else{
  	return "No user found";
  }
};

displayId = function(userId) {
	check(userId, NonEmptyString);
	var myUser = Meteor.users.findOne(userId);
	if(!myUser)
		throw new Meteor.Error(413, "Unable to locate the user");
	return displayName(myUser);
};

displayBetName = function(bet) {
	if(bet){
		return bet.description;
	}
	else{
		return "No bet found";
	}
};

displayBetId = function(betId) {
	check(betId, NonEmptyString);
	var bet = Bets.findOne(betId);
	if(!bet)
		throw new Meteor.Error(413, "Unable to find bet: "+betId);
	return displayBetName(bet);
};

displayBetAmount = function(user, bet) {
	if(bet && user){
		for(var i=0, l=user.bets.length; i<l; i++){
			if(user.bets[i].bid === bet._id){
				return user.bets[i].amount;
			}
		}
		return "";
	}
	return "";
};



var contactEmail = function (user) {
  if (user.emails && user.emails.length)
    return user.emails[0].address;
  if (user.services && user.services.facebook && user.services.facebook.email)
    return user.services.facebook.email;
  return null;
};


