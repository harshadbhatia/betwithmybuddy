//frib - client

Meteor.subscribe("everyone");
Meteor.subscribe("bets");

Template.page.showAddFriendDialog = function() {
	return Session.get("showAddFriendDialog");
};

Template.page.showMakeBetDialog = function() {
	return Session.get("showMakeBetDialog");
};

Template.page.canAdd = function() {
	return true; //TODO: always enable 'add people' else user might get confused
};

Template.page.events({
	'click .add': function(){
		openAddFriendDialog();
		return false;
	},
	'click .unnotify': function(){
		var self = Meteor.user();
		if(self){
			Meteor.call("unnotify", self._id, objToString(this));
		}
		return false;
	}

});

Template.page.userGreeting = function(){
	var self = Meteor.user();
	if (self) {
		return displayName(self);
	}
	else{
		return "";
	}
};
Template.page.userBalance = function(){
	var self = Meteor.user();
	if(self){
		return self.balance;
	}
	else{
		return "";
	}
};

Template.page.userRating = function(){
	var self = Meteor.user();
	if(self){
		return self.rating;
	}
	else{
		return "";
	}
};

Template.page.notify = function() {
	return this;
};

Template.page.notifications = function() {
	var self = Meteor.user();
	if(self){
		return self.notifications;
	}
	else{
		return [];
	}

};

var objToString = function(obj){
	var ret = "";
	for(var k in obj){
		if(obj.hasOwnProperty(k)){
			ret += obj[k];
		}
	}
	return ret;
};

var openAddFriendDialog = function () {
  Session.set("showAddFriendDialog", true);
};

var inReqsUsers = function(){
	var self = Meteor.user();
	if(self && self.inReqs){
		var reqs = [];
		var req;
		var ids = self.inReqs;
		var len = ids.length;
		for(var i=0; i<len; i++){
			req = Meteor.users.findOne(ids[i]);
			if(req){
				reqs.push(req);
			}
		}
		return reqs;
	}
	else{
		return [];
	}
};

var objectStruct = function(obj){
	for (var k in obj) {
  		if (obj.hasOwnProperty(k)) {
    		var value = obj[k];
    		//console.log("property name is " + k + " value is " + value);
  		}
	}
};

var outReqsUsers = function(){
	var self = Meteor.user();
	if(self && self.outReqs){
		var reqs = [];
		var req;
		var ids = self.outReqs;
		var len = ids.length;
		for(var i=0; i<len; i++){
			req = Meteor.users.findOne(ids[i]);
			if(req){
				reqs.push(req);
			}
		}
		//console.log(reqs);
		return reqs;
	}
	else{
		return [];
	}
};

var friendsUsers = function(){
	var self = Meteor.user();
	if(self && self.friends){
		var reqs = [];
		var req;
		var ids = self.friends;
		var len = ids.length;
		for(var i=0; i<len; i++){
			req = Meteor.users.findOne(ids[i]);
			if(req){
				reqs.push(req);
			}
		}
		//console.log(reqs);
		return reqs;
	}
	else{
		return [];
	}
};
var confirmFriendRequest = function(senderId){
	var self = Meteor.user();
	var sender = Meteor.users.findOne(senderId);
	var senderMessage = "";
	var selfMessage = "";
	if(sender.flen >= sender.flimit){
		//notify both receiver & sender
		senderMessage += "Your quota is full. Unfriend someone and add ";
		senderMessage += displayName(self) + " again\n";
		selfMessage += displayName(sender)+" has been notified to add you again ";
		if(self.flen >= self.flimit){
			selfMessage += " as his quota is also full like you.\n";
		}
		else{
			selfMessage += " as his quota is full.\n";
		}
		Meteor.call("rejectFriendReq", senderId);
		Meteor.call("notify", senderId, senderMessage);
		Meteor.call("notify", self._id, selfMessage);
	}
	else if(self.flen >= self.flimit){
		selfMessage += "Your quota is full. Unfriend someone before confirming ";
		selfMessage += displayName(sender) + "\n";
		Meteor.call("notify", self._id, selfMessage);
	}
	else{
		selfMessage += "Woo, you and "+displayName(sender)+" are now friends\n";
		senderMessage += "Woo, you and "+displayName(self)+" are now friends\n";
		Meteor.call("confirmFriendReq", senderId);
		Meteor.call("notify", senderId, senderMessage);
		Meteor.call("notify", self._id, selfMessage);
	}
	return;

};


var origFriendReqId = null;

///////////////////////////////////////////////////////////////////////////////
// Add friend dialog
Template.addFriendDialog.events({
	'click .add': function(event, template){
		addFriend(this._id);
	},
	'click .unfriend': function(event, template){
		Meteor.call("removeFriend", this._id);
		if (origFriendReqId){
			Meteor.call("sendFriendReq", origFriendReqId);
		}
		else{
			Session.set("unfriendSwitch", false);
			throw new Meteor.Error(403, "Unable to locate the original friend request.");
		}
		Session.set("unfriendSwitch", false);
	},
	'click .done': function(event, template){
		Session.set("showAddFriendDialog", false);
		Session.set("unfriendSwitch", false);
		return false;
	}
});

Template.addFriendDialog.unfriendSwitch = function(){
	return Session.get("unfriendSwitch");
};

Template.addFriendDialog.toUnfriend = function(){
	return friendsUsers;
};

var addFriend = function(friendId){
	var self = Meteor.user();
	if (self.flen < self.flimit){
		Meteor.call("sendFriendReq", friendId);
	}
	else{
		origFriendReqId = friendId;
		Session.set("unfriendSwitch", true);
	}
};

Template.addFriendDialog.unadded = function(){
	var self = Meteor.user();
	if(self){
		return Meteor.users.find({$nor: [{_id: {$in: self.friends}},
										 {_id: {$in: self.outReqs}},
										 {_id: {$in: self.inReqs}},
										 {_id: self._id}]});
	}
	else{
		return [];
	}
};

Template.addFriendDialog.displayName = function(){
	return displayName(this);
};
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// User details
Template.userDetails.friends = function(){
	var self = Meteor.user();
	if(self && self.friends){
		var friends = [];
		var friend;
		var ids = self.friends;
		var len = ids.length;
		for (var i=0; i<len; i++){
			friend = Meteor.users.findOne(ids[i]);
			if(friend){
				friends.push(friend);
			}
		}
		return friends;
	}
	else{
		return [];
	}
};

Template.userDetails.myFriend = function(){
	return displayName(this);
};

Template.userDetails.inBetReqs = function(){
	var self = Meteor.user();
	if(self && self.betReqs){
		var inReq;
		var inReqs = [];
		var betRequests = self.betReqs;
		for(var i=0, len=betRequests.length; i<len; i++){
			inReq = Bets.findOne(betRequests[i]);
			if(inReq){
				inReqs.push(inReq);
			}
		}
		return inReqs;
	}
	else{
		return [];
	}
};

Template.userDetails.betReceived = function(){
	return displayBetName(this);
};

Template.userDetails.activeBets = function(){
	var self = Meteor.user();
	if(self && self.bets){
		var myActiveBet;
		var myActiveBets = [];
		var myBets = self.bets;
		for(var i=0, l=myBets.length; i<l; i++){
			myActiveBet = Bets.findOne(
								{
									_id: myBets[i].bid,
									active: true
								}
						  );
			if(myActiveBet){
				//console.log("myActiveBet = "+displayBetName(myActiveBet));
				myActiveBets.push(myActiveBet);
			}
		}
		return myActiveBets;
	}
	else{
		return [];
	}
};

Template.userDetails.myActiveBet = function(){
	var self = Meteor.user();
	if(self){
		return displayBetName(this);
	}
	else{
		return "";
	}
};

Template.userDetails.activeBetAmount = function(){
	var self = Meteor.user();
	if(self){
		return displayBetAmount(self, this);
	}
	else{
		return "";
	}
};

Template.userDetails.activeBetComm = function(){
	var self = Meteor.user();
	if(self){
		return getUserCommission(self._id, this._id);
	}
	else{
		return "";
	}
};

Template.userDetails.alreadyVoted = function(){
	var bothVotes = this.vA.concat(this.vB);
	var self = Meteor.user();
	var voted = false;
	for(var i=0, len=bothVotes.length; i<len; i++){
		if(bothVotes[i] === self._id){
			voted = true;
			break;
		}
	}
	return voted;
};

Template.userDetails.alreadyPaid = function(){
	var self = Meteor.user();
	if(this.result.length===0){
		return false;
	}
	var paid = false;
	for(var i=0, l=self.bets.length; i<l; i++){
		if(self.bets[i].bid===this._id && self.bets[i].amount === -1){
			paid = true;
			break;
		}
	}
	return paid;
};

Template.userDetails.resultDecided = function(){
	if(this.result.length !==0 ){
		var losingSide;
		var self = Meteor.user();
		if(this.result === "a"){
			losingSide = this.sideB;
		}
		else{
			losingSide = this.sideA;
		}
		for(var i=0, l=losingSide.length; i<l; i++){
			if(losingSide[i] === self._id){
				return true;
			}
		}
		return false;
	}
	else{
		return false;
	}
};

Template.userDetails.events({
	'click .unfriend': function(event, template){
		Meteor.call("removeFriend", this._id);
		return false;
	},
	'click .bet': function(event, template){
		openMakeBetDialog();
		return false;
	},
	'click .sideA': function(event, template){
		Meteor.call("acceptBetReq", this._id, "a");
		return false;
	},
	'click .sideB': function(event, template){
		Meteor.call("acceptBetReq", this._id, "b");
		return false;
	},
	'click .bet_reject': function(event, template){
		Meteor.call("rejectBetReq", this._id);
		return false;
	},
	'click .winA': function(event, template){
		Meteor.call("voteWinner", this._id, "a");
	},
	'click .winB': function(event, template){
		Meteor.call("voteWinner", this._id, "b");
	},
	'click .payBet': function(event, template){
		Meteor.call("payBet", this._id);
	},
});

var openMakeBetDialog = function(){
	Session.set("showMakeBetDialog", true);
	Session.set("invitationCache_session", []);
	Session.set("createError", null);
};
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Make bet dialog

Template.makeBetDialog.events({
	'click .done': function(event, template){
		Session.set("showMakeBetDialog", false);
		Session.set("invitationCache_session", []);
		return false;
	},
	'click .itIsOn': function(event, template){
		//Meteor.call("createBet");
		var invitations = Session.get("invitationCache_session");
		var desc = template.find(".desc").value;
		var myStake = template.find(".stake").value;

		if(invitations.length && desc.length && myStake.length){
			/*for (var i = 0; i < invitations.length; i++) {
				console.log("invitations="+displayId(invitations[i]));
			};
			console.log("description="+description);
			console.log("stake="+stake);*/
			//var self = Meteor.user();
			//invitations.push(self._id);
			var stakeCast = Number(myStake);
			Meteor.call("createNewBet",  {
					public: true,
					invited: invitations,
					description: desc,
					stake: stakeCast
				}, function(error, bet){
					if(!error){
						console.log("bet successfully created: "+bet);
					}
			});
			Session.set("showMakeBetDialog", false);
		}
		else{
			Session.set("createError",
						"You need atleast one other person, description and amount to make a bet, or why bother?");
		}
		Session.set("invitationCache_session", []);
		return false;
	},
	'click .invite': function(event, template){
		var invitationCache = Session.get("invitationCache_session");
		var inviteeId = this._id;
		if(_.find(invitationCache, function(i){
				 					return i === inviteeId;})){
			invitationCache = _.without(invitationCache, inviteeId);
			Session.set("invitationCache_session", invitationCache);
		}
		else{
			invitationCache.push(inviteeId);
			Session.set("invitationCache_session", invitationCache);
		}
	},
});

Template.makeBetDialog.uninvited = function() {
	var self = Meteor.user();
	if(self){
		return Meteor.users.find({_id: {$not: self._id}});
	}
	else{
		return [];
	}
};

Template.makeBetDialog.displayName = function() {
	return displayName(this);
};

Template.makeBetDialog.invitedCache = function(person){
	var myCache = Session.get("invitationCache_session");
	return (_.find(myCache, function(i){
			 				return i === person._id;})) ? true : false;
};

Template.makeBetDialog.error = function(){
	return Session.get("createError");
};

///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Requests
Template.requests.inReqs = function(){
	return inReqsUsers;
};

Template.requests.received = function(){
	return displayName(this);
};

Template.requests.outReqs = function(){
	return outReqsUsers;
};

Template.requests.sent = function(){
	return displayName(this);
};

Template.requests.outBetReqs = function(){
	var self = Meteor.user();
	if(self && self.betReqsSent){
		var outBet;
		var outBets = [];
		var betsSent = self.betReqsSent;
		for (var i=0, len=betsSent.length; i<len; i++) {
			outBet = Bets.findOne(betsSent[i]);
			if(outBet){
				outBets.push(outBet);
			}
		}

		return outBets;
	}
	else{
		return [];
	}
};

Template.requests.betSent = function(){
	return displayBetName(this);
};

Template.requests.events({
	'click .friend_yes': function(event, template){
		confirmFriendRequest(this._id);
	},
	'click .friend_no': function(event, template){
		Meteor.call("rejectFriendReq", this._id);
	}
});
///////////////////////////////////////////////////////////////////////////////


