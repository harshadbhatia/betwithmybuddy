//frib server

Meteor.publish("everyone", function(){
	if (this.userId) {
		return Meteor.users.find({}); //modify in future, to return user's friends only
	}
	else{
		return null;
	}
});

Meteor.publish("bets", function(){
	if(this.userId){
		return Bets.find({}); //modify in future to return bets based on some criteria
	}
	else{
		return null;
	}
});
